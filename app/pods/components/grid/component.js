import Component from '@ember/component'
import {PropTypes} from 'ember-prop-types'
import { action } from '@ember/object'
import {cloneDeep} from 'lodash'
import computed from 'ember-macro-helpers/computed'
import {set} from '@ember/object'

const STATUS_AVAILABLE = 'available'

export default Component.extend({
  classNames: ['grid-component'],
  // == Properties ============================================================
  gridOptions: [],
  selectedItems: [],
  selectedEntries: [],
  // == PropTypes =============================================================
  propTypes: {
    gridDefs: PropTypes.array.isRequired
  },

  // == Computed Properties ===================================================
  selectedLabel: computed('selectedItems.[]', function (selectedItems) {
    return `${selectedItems.length} selected`
  }).readOnly(),

  isIndeterminate: computed('gridOptions', 'selectedItems.[]', function (gridOptions, selectedItems) {
    const selectedLength = selectedItems.length
    return selectedLength > 0 && selectedLength < gridOptions.length
  }).readOnly(),

  downloadButtonDisabled: computed('selectedItems.[]', function (selectedItems) {
    return !selectedItems.some(item => item.status === STATUS_AVAILABLE)
  }).readOnly(),

  isParentCheckBoxChecked: computed('gridOptions', 'selectedItems.[]', function (gridOptions, selectedItems) {
    return gridOptions.length === selectedItems.length
  }).readOnly(),

  // == Functions =============================================================
  init () {
    this._super(...arguments)
    this.gridOptions = cloneDeep(this.gridDefs)
    this.gridOptions.forEach(gridOption => gridOption['checked'] = false)
  },

  clearEntries () {
    this.setProperties({
      selectedEntries: [],
      selectedItems: []
    })
  },

  populateEntriesList () {
    if (this.selectedItems.length < this.gridOptions.length) {
      this.clearEntries()
      this.selectedEntries = [...this.gridOptions]
      this.selectedItems.setObjects(this.selectedEntries)
      this.gridOptions.forEach(gridOption => {
        set(gridOption, 'checked', true)
      })
      return
    }
    this.clearEntries()
    this.gridOptions.forEach(gridOption => {
      set(gridOption, 'checked', false)
    })
  },

  // == Actions ===============================================================
  @action
  onOptionSelected (value) {
    const index = this.selectedEntries.findIndex(item => item.name === value.name)
    if (index >= 0) {
      this.selectedEntries.splice(index, 1)
      this.selectedItems.setObjects(this.selectedEntries)
      return
    }
    this.selectedEntries.push(value)
    this.selectedItems.setObjects(this.selectedEntries)
  },

  @action
  onDownloadClick () {
    const alertText = this.selectedItems.filter(item => item.status === STATUS_AVAILABLE).reduce((acc, item) => {
      return `${acc} ${item.device} ${item.path}`
    }, '')
    alert(`The following would be downloaded to the system on confirm. ${alertText}`)
  },

  @action
  checkAll () {
    this.populateEntriesList()
  }
})
