import Component from '@ember/component'
import {PropTypes} from 'ember-prop-types'
import computed from 'ember-macro-helpers/computed'
import { action } from '@ember/object'
import {set} from '@ember/object'

export default Component.extend({
  // == Properties ============================================================
  tagName: '',
  // == PropTypes =============================================================
  propTypes: {
    gridOption: PropTypes.object.isRequired,
    selection: PropTypes.func.isRequired
  },

  // == Computed Properties ===================================================
  rowData: computed('gridOption', function (gridOption) {
    return Object.keys(gridOption).filter(optionKey => optionKey !== 'checked').map(key => {
      return {
        key,
        value: gridOption[key]
      }
    })
  }).readOnly(),

  // == Actions ===============================================================
  @action
  checkboxClicked () {
    set(this.gridOption, 'checked', !this.gridOption.checked)
    this.selection(this.gridOption)
  }
})
