import Component from '@ember/component'
import {PropTypes} from 'ember-prop-types'
import computed from 'ember-macro-helpers/computed'
import {capitalize} from 'lodash'

export default Component.extend({
  classNames: ['status-column'],
  // == Properties ============================================================
  tagName: '',
  // == PropTypes =============================================================
  propTypes: {
    status: PropTypes.string.isRequired
  },

  statusText: computed('status', function (status) {
    return capitalize(status)
  }).readOnly(),
})
