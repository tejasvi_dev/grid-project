import Component from '@ember/component'
import {PropTypes} from 'ember-prop-types'
import computed from 'ember-macro-helpers/computed'
import {capitalize} from 'lodash'

export default Component.extend({
  // == Properties ============================================================
  tagName: '',
  // == PropTypes =============================================================
  propTypes: {
    gridOptions: PropTypes.array.isRequired
  },

  // == Computed Properties ===================================================
  columnLabels: computed('gridOptions', function (gridOptions) {
    return Object.keys(gridOptions[0]).filter(gridKey => gridKey !== 'checked').map(key => capitalize(key))
  }).readOnly()
})
