import {render, settled} from '@ember/test-helpers'
import {expect} from 'chai'
import {integration} from 'ember-test-utils/test-support/setup-component-test'
import hbs from 'htmlbars-inline-precompile'
import {beforeEach, describe, it} from 'mocha'

const test = integration('grid')

describe(test.label, function () {
  test.setup()

  describe('after render', function () {
    beforeEach(async function () {
      this.set('gridDefs', [
        { name: 'smss.exe', device: 'Stark', path: '\\Device\\HarddiskVolume2\\Windows\\System32\\smss.exe', status: 'scheduled' },
        { name: 'netsh.exe', device: 'Targaryen', path: '\\Device\\HarddiskVolume2\\Windows\\System32\\netsh.exe', status: 'available' }
      ])

      await render(hbs`
        {{grid gridDefs=gridDefs}}
      `)
    })

    it('should one parent checkbox and 2 child checkboxes the all the choices', function () {
      const rows = document.querySelectorAll('.checkbox-large')
      expect(rows.length).to.equal(3)
    })

    it('should select all child checkboxes when parent checkbox is clicked', async function () {
      const row = document.querySelector('.checkbox-large')
      row.click()
      await settled()
      const allRows = document.querySelectorAll('.checkbox-large')
      expect(allRows[2].checked).to.equal(true)
      expect(allRows[1].checked).to.equal(true)
    })

    it('should have button disabled when row with scheduled state is selected', async function () {
      const rows = document.querySelectorAll('.checkbox-large')
      rows[1].click()
      await settled()
      const button = document.querySelector('.download-button')
      expect(button.disabled).to.equal(true)
    })

    it('should have button enabled when row with available state is selected', async function () {
      const rows = document.querySelectorAll('.checkbox-large')
      rows[2].click()
      await settled()
      const button = document.querySelector('.download-button')
      expect(button.disabled).to.equal(false)
    })
  })
})
