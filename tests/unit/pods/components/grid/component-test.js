import {unit} from 'ember-test-utils/test-support/setup-component-test'
import {afterEach, beforeEach, describe, it} from 'mocha'
import sinon from 'sinon'
import {expect} from 'chai'

const test = unit('grid')

describe(test.label, function () {
  test.setup()

  describe('component', function () {
    let sandbox, component

    beforeEach(function () {
      component = this.owner.factoryFor('component:grid').create({
        gridDefs: [{ name: 'smss.exe', device: 'Stark', path: '\\Device\\HarddiskVolume2\\Windows\\System32\\smss.exe', status: 'scheduled' },
          { name: 'netsh.exe', device: 'Targaryen', path: '\\Device\\HarddiskVolume2\\Windows\\System32\\netsh.exe', status: 'available' }]
      })
      sandbox = sinon.createSandbox()
    })

    afterEach(function () {
      component = null
      sandbox.restore()
    })

    describe('computed: selectedLabel', function () {
      describe('When length of selectedItems is 0', function () {
        beforeEach(function () {
          component.set('selectedItems', [])
        })
        it('should return 0 selected as label', function () {
          expect(component.get('selectedLabel')).to.equal('0 selected')
        })
      })

      describe('When length of selectedItems is 2', function () {
        beforeEach(function () {
          component.set('selectedItems', [{name: 'smss.exe', device: 'Stark'}, {name: 'netsh.exe', device: 'Targaryen'}])
        })

        it('should return 2 selected as label', function () {
          expect(component.get('selectedLabel')).to.equal('2 selected')
        })
      })
    })

    describe('computed: isParentCheckBoxChecked', function () {
      describe('When length of selectedItems and gridOptions is same', function () {
        beforeEach(function () {
          component.setProperties({
            selectedItems: [{name: 'smss.exe', device: 'Stark'}],
            gridOptions: [{name: 'smss.exe', device: 'Stark'}]
          })
        })
        it('should return true', function () {
          expect(component.get('isParentCheckBoxChecked')).to.equal(true)
        })
      })

      describe('When length of selectedItems and gridOptions is not same', function () {
        beforeEach(function () {
          component.setProperties({
            selectedItems: [],
            gridOptions: {name: 'smss.exe', device: 'Stark'}
          })
        })
        it('should return false', function () {
          expect(component.get('isParentCheckBoxChecked')).to.equal(false)
        })
      })
    })

    describe('computed: isIndeterminate', function () {
      describe('When length of selectedItems is 0', function () {
        beforeEach(function () {
          component.set('selectedItems', [])
        })
        it('should return false', function () {
          expect(component.get('isIndeterminate')).to.equal(false)
        })
      })

      describe('When length of selectedItems is 1 and gridOptions length is 2', function () {
        beforeEach(function () {
          component.setProperties({
            selectedItems: [{name: 'smss.exe', device: 'Stark'}],
            gridOptions: [{name: 'smss.exe', device: 'Stark'}, {name: 'netsh.exe', device: 'Targaryen'}]
          })
        })

        it('should return true', function () {
          expect(component.get('isIndeterminate')).to.equal(true)
        })
      })

      describe('When length of selectedItems is 2 and gridOptions length is 2', function () {
        beforeEach(function () {
          component.setProperties({
            selectedItems: [{name: 'smss.exe', device: 'Stark'}, {name: 'netsh.exe', device: 'Targaryen'}],
            gridOptions: [{name: 'smss.exe', device: 'Stark'}, {name: 'netsh.exe', device: 'Targaryen'}]
          })
        })

        it('should return true', function () {
          expect(component.get('isIndeterminate')).to.equal(false)
        })
      })
    })

    describe('computed: downloadButtonDisabled', function () {
      describe('When length of selectedItems is 0', function () {
        beforeEach(function () {
          component.set('selectedItems', [])
        })
        it('should return true', function () {
          expect(component.get('downloadButtonDisabled')).to.equal(true)
        })
      })

      describe('When length of selectedItems is 1 status is scheduled', function () {
        beforeEach(function () {
          component.setProperties({
            selectedItems: [{name: 'smss.exe', device: 'Stark', status: 'scheduled'}]
          })
        })

        it('should return true', function () {
          expect(component.get('downloadButtonDisabled')).to.equal(true)
        })
      })

      describe('When length of selectedItems is 1 status is available', function () {
        beforeEach(function () {
          component.setProperties({
            selectedItems: [{name: 'smss.exe', device: 'Stark', status: 'available'}]
          })
        })

        it('should return false', function () {
          expect(component.get('downloadButtonDisabled')).to.equal(false)
        })
      })

      describe('When length of selectedItems is 2 with one status scheduled and one as available', function () {
        beforeEach(function () {
          component.set('selectedItems', [{name: 'smss.exe', device: 'Stark', status: 'available'},
            {name: 'netsh.exe', device: 'Targaryen', status: 'scheduled'}])
        })

        it('should return false', function () {
          expect(component.get('isIndeterminate')).to.equal(false)
        })
      })
    })

    describe('action: onOptionSelected', function () {
      describe('When a row is selected', function () {
        beforeEach(function () {
          component.setProperties({selectedEntries: [], selectedItems: []})
          component.send('onOptionSelected', {name: 'smss.exe', device: 'Stark', status: 'available'})
        })
        it('should add this object to the selected list', function () {
          expect(component.get('selectedItems').length).to.equal(1)
        })
      })

      describe('When a row is de-selected', function () {
        beforeEach(function () {
          component.setProperties({selectedEntries: [{name: 'smss.exe', device: 'Stark', status: 'available'}],
            selectedItems: [{name: 'smss.exe', device: 'Stark', status: 'available'}]})
          component.send('onOptionSelected', {name: 'smss.exe', device: 'Stark', status: 'available'})
        })
        it('should remove this object from the selected list', function () {
          expect(component.get('selectedItems').length).to.equal(0)
        })
      })
    })

    describe('action: checkAll', function () {
      describe('When rows are partially selected', function () {
        beforeEach(function () {
          component.setProperties({selectedEntries: [{name: 'smss.exe', device: 'Stark', status: 'available'}],
            selectedItems: [{name: 'smss.exe', device: 'Stark', status: 'available'}],
            gridOptions: [{name: 'smss.exe', device: 'Stark', status: 'available'}, {name: 'netsh.exe', device: 'Targaryen', status: 'scheduled'}]
          })
          component.send('checkAll')
        })
        it('should select all the rows', function () {
          expect(component.get('selectedItems').length).to.equal(2)
        })
      })

      describe('When all rows are selected', function () {
        beforeEach(function () {
          component.setProperties({selectedEntries: [{name: 'smss.exe', device: 'Stark', status: 'available'}],
            selectedItems: [{name: 'smss.exe', device: 'Stark', status: 'available'}],
            gridOptions: [{name: 'smss.exe', device: 'Stark', status: 'available'}]
          })
          component.send('checkAll')
        })
        it('should un-select all the rows', function () {
          expect(component.get('selectedItems').length).to.equal(0)
        })
      })

      describe('When no rows are selected', function () {
        beforeEach(function () {
          component.setProperties({selectedEntries: [],
            selectedItems: [],
            gridOptions: [{name: 'smss.exe', device: 'Stark', status: 'available'}]
          })
          component.send('checkAll')
        })
        it('should select all the rows', function () {
          expect(component.get('selectedItems').length).to.equal(1)
        })
      })
    })
  })
})
