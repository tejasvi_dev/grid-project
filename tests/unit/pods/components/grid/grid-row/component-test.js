import {unit} from 'ember-test-utils/test-support/setup-component-test'
import {afterEach, beforeEach, describe, it} from 'mocha'
import sinon from 'sinon'
import {expect} from 'chai'

const test = unit('grid/grid-row')

describe(test.label, function () {
  test.setup()

  describe('component', function () {
    let sandbox, component

    beforeEach(function () {
      component = this.owner.factoryFor('component:grid/grid-row').create({
        gridOption: {
          name: 'smss.exe',
          device: 'Stark',
          path: '\\Device\\HarddiskVolume2\\Windows\\System32\\smss.exe',
          status: 'scheduled',
          checked: true
        }
      })
      sandbox = sinon.createSandbox()
    })

    afterEach(function () {
      component = null
      sandbox.restore()
    })

    describe('computed: rowData', function () {
      describe('Will have proper rowData', function () {
        it('should return rowData with key and value', function () {
          expect(component.get('rowData')).to.eql([
            {
              key: 'name',
              value: 'smss.exe'
            },
            {
              key: 'device',
              value: 'Stark'
            },
            {
              key: 'path',
              value: '\\Device\\HarddiskVolume2\\Windows\\System32\\smss.exe'
            },
            {
              key: 'status',
              value: 'scheduled'
            }
          ])
        })
      })
    })
    describe('action: checkboxClicked', function () {
      describe('when a row is selected', function () {
        beforeEach(function () {
          component.setProperties({
            gridOption: {name: 'smss.exe', device: 'Stark', status: 'available', checked: false},
            selection: () => {}
          })
          component.send('checkboxClicked')
        })
        it('should toggle the checked attribute', function () {
          expect(component.get('gridOption')).to.eql({
            name: 'smss.exe', device: 'Stark', status: 'available', checked: true})
        })
      })

      describe('when a row is de-selected', function () {
        beforeEach(function () {
          component.setProperties({
            gridOption: {name: 'smss.exe', device: 'Stark', status: 'available', checked: true},
            selection: () => {}
          })
          component.send('checkboxClicked')
        })
        it('should toggle the checked attribute', function () {
          expect(component.get('gridOption')).to.eql({
            name: 'smss.exe', device: 'Stark', status: 'available', checked: false})
        })
      })
    })
  })
})
